import java.util.ArrayList;
import java.util.Collections;
// import java.util.Comparator;
import java.util.List;

public class CollectionsSort
{
	public static void main(String[] args)
	{
		if (args.length == 0)
		{
			System.out.println("\nUsage: java CollectionsSort # # #");
			System.exit(0);
		}

		List<Integer> list = new ArrayList<>(args.length); // Diamond syntax
		for (String s : args)
			try
			{
				list.add(Integer.parseInt(s));
			}
			catch (NumberFormatException e)
			{
				System.out.println("\nEnter only integers.\n");
				System.out.println("Example: java CollectionsSort 100 10 1");				
				System.exit(0);
			}

		// Anonymous Class
		// Collections.sort(list, new Comparator<Integer>()
		// {
		// 	public int compare(Integer o1, Integer o2)
		// 	{
		// 		return (o1 < o2) ? -1 : ((o1 == o2) ? 0 : 1);
		// 	}
		// });

		// Java 8 Lambda Expression with functional interface
		Collections.sort(list, (o1, o2) -> { return (o1 < o2) ? -1 : ((o1 == o2) ? 0 : 1); });

		System.out.println();
		System.out.println(list);
	}
}
