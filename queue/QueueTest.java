public class QueueTest
{
	public static void main(String[] args)
	{
		Queue q = new Queue();

		System.out.println("\nA queue is a First-In-First-Out (FIFO) data structure.\n" + 
			"This demo is not very interesting.\n");
		
		System.out.println("Inserting a sequence of numbers into a queue:");
		
		for (int itemNumber = 0; itemNumber < 10; itemNumber++)
		{
			q.insert(itemNumber);
			System.out.print(itemNumber + " ");
		}
		
		System.out.println("\n\nRemoving a sequence of numbers from a queue:");
		
		while (!q.isEmpty())
		{
			Item i = q.remove();
			System.out.print(i.id + " ");
		}
		
		System.out.println();
	}
}
