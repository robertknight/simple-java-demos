public class Queue
{
	private Item first = null;
	private Item last = null;
	
	public void insert(int itemNumber)
	{
		Item newItem = new Item(itemNumber);
	
		if (isEmpty())
		{
			first = newItem;
			last = first;
		}
		else
		{
			last.link = newItem;
			last = newItem;
		}		
	}
	
	public Item remove()
	{			
		Item item = new Item(first.id);
		first = first.link;
		
		return item;
	}
	
	public boolean isEmpty()
	{
		return (first == null) ? true : false;
	}
}
