public class Mergesort
{
	private int[] buffer = null;
	private boolean isBufferSized = false;

	public void mergesort(int low, int high, int[] list)
	{	
		if (!isBufferSized)
		{
			buffer = new int[list.length];
			isBufferSized = true;
		}
	
		if (low < high)
		{
			int middle = (low + high) / 2;
			
			mergesort(low, middle, list);
			mergesort(middle + 1, high, list);
			
			merge(low, middle, high, list);
		}
	}
	
	private void merge(int low, int middle, int high, int[] list)
	{
		int i = low;
		int j = i;
		int k = middle + 1;
		
		for (int l = low; l <= high; l++)
			buffer[l] = list[l];
				
		while (i <= middle && k <= high)
		{
			if (buffer[i] <= buffer[k])
				list[j++] = buffer[i++];
			else
				list[j++] = buffer[k++];
		}
		
		while (i <= middle)
			list[j++] = buffer[i++];
	}
}
