public class MergesortTest
{
	public static void main(String[] args)
	{
		if (args.length == 0)
		{
			System.out.println("\nUsage: java MergesortTest # # #");
			System.exit(0);
		}

		Mergesort ms = new Mergesort();
		int[] list = new int[args.length];

		for (int i = 0; i < args.length; i++)
			try
			{
				list[i] = Integer.parseInt(args[i]);
			}
			catch (NumberFormatException e)
			{
				System.out.println("\nEnter only integers.\n");
				System.out.println("Example: java MergesortTest 100 10 1");				
				System.exit(0);
			}
		
		ms.mergesort(0, list.length-1, list);

		System.out.println("\nMerge sort has an average and worst case run-time of O(nlogn).\n");
		
		for (int i : list)
			System.out.print(i + " ");
		System.out.println();
	}
}
