public class InsertionSortTest
{
	public static void main(String[] args)
	{
		if (args.length == 0)
		{
			System.out.println("\nUsage: java InsertionSortTest # # #");
			System.exit(0);
		}

		int[] list = new int[args.length];

		for (int i = 0; i < args.length; i++)
			try
			{
				list[i] = Integer.parseInt(args[i]);
			}
			catch (NumberFormatException e)
			{
				System.out.println("\nEnter only integers.\n");
				System.out.println("Example: java InsertionSortTest 100 10 1");				
				System.exit(0);
			}

		InsertionSort is = new InsertionSort(list);

		System.out.println("\nInsertion sort has an average and worst case run-time of O(n^2).\n");

		for (int i : list)
			System.out.print(i + " ");
		System.out.println();
	}
}
