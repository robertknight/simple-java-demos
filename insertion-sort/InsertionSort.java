public class InsertionSort
{
	public InsertionSort(int list[])
	{
		for (int i = 1; i < list.length; i++)
		{
			int j = i;
			int k = list[i];
			
			while (k < list[j-1])
			{
				j--;
				
				if (j == 0)
					break;
			}
				
			if (j < i)
			{
				for (int l = i; l > j; l--)
					list[l] = list[l-1];
					
				list[j] = k;
			}
		}	
	}
}
