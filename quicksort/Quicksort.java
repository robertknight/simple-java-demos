public class Quicksort
{	
	public void quicksort(int left, int right, int[] list)
	{
		if (list.length <= 1)
			return;
		
		int i = left;
		int j = right;
		int pivot = list[left + (right-left) / 2];
		
		while (i <= j)
		{
			while (list[i] < pivot)
				i++;
			while (list[j] > pivot)
				j--;
			
			if (i <= j)
			{
				int tmp = list[i];
				
				list[i++] = list[j];
				list[j--] = tmp;
			}
		}
		
		if (left < j)
			quicksort(left, j, list);
		if (i < right)
			quicksort(i, right, list);
	}
}
