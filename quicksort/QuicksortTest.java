public class QuicksortTest
{
	public static void main(String[] args)
	{
		if (args.length == 0)
		{
			System.out.println("\nUsage: java QuicksortTest # # #");
			System.exit(0);
		}
		
		Quicksort qs = new Quicksort();
		int[] list = new int[args.length];

		for (int i = 0; i < args.length; i++)
			try
			{
				list[i] = Integer.parseInt(args[i]);
			}
			catch (NumberFormatException e)
			{
				System.out.println("\nEnter only integers.\n");
				System.out.println("Example: java QuicksortTest 100 10 1");				
				System.exit(0);
			}
		
		qs.quicksort(0, list.length-1, list);
		
		System.out.println("\nQuicksort has an average run-time of O(nlogn).");
		System.out.println("Its worst case run-time is O(n^2).\n");

		for (int i : list)
			System.out.print(i + " ");
		System.out.println();
	}
}
