public class Stack
{
	private Item top = null; // defaults to null without assignment. explicit assignment
	
	public void push(String s)
	{
		Item newItem = new Item(s);
		
		if (isEmpty())
			top = newItem;
		else
		{
			newItem.link = top;
			top = newItem;
		}
	}
	
	public Item pop()
	{
		Item item = new Item(top.text);
		top = top.link;
		
		return item;
	}
	
	public boolean isEmpty()
	{
		return (top == null) ? true : false;
	}
}
