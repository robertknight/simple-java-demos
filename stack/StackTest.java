public class StackTest
{
	// public static void main(String[] args)
	public static void main(String... args) // varargs style
	{
		if (args.length == 0)
		{
			System.out.println("\nUsage: java Stacktest arg1 arg2 ... argN");
			System.exit(0);
		}	

		Stack stack = new Stack();
		
		for (String s : args)
			stack.push(s);
		
		System.out.println("A stack is a Last-In-First-Out (LIFO) data structure.\n");

		while (!stack.isEmpty())
		{
			Item item = stack.pop();
			System.out.println(item.text);
		}
	}
}
