public class HeapsortTest
{
	public static void main(String[] args)
	{		
		if (args.length == 0)
		{
			System.out.println("\nUsage: java HeapsortTest # # #");
			System.exit(0);
		}
		
		int[] list = new int[args.length];

		for (int i = 0; i < args.length; i++)
			try
			{
				list[i] = Integer.parseInt(args[i]);
			}
			catch (NumberFormatException e)
			{
				System.out.println("\nEnter only integers.\n");
				System.out.println("Example: java HeapsortTest 100 10 1");				
				System.exit(0);
			}

		Heapsort hs = new Heapsort(list);

		System.out.println("\nHeapsort has an average and worst case run-time of O(nlogn).\n");
		
		for (int i : list)
			System.out.print(i + " ");
		System.out.println();
	}
}
