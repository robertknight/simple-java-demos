public class Heapsort
{
	public Heapsort(int[] list)
	{
		int end = list.length - 1;
		
		heapify(end, list);
		
		while (end > 0)
		{
			int tmp = list[0];
			
			list[0] = list[end];
			list[end] = tmp;
			
			siftDown(0, --end, list);
		}
	}	
	
	private void heapify(int end, int[] list)
	{
		int begin = (end - 2) / 2;
		
		while (begin >= 0)
			siftDown(begin--, end, list);
	}
	
	private void siftDown(int begin, int end, int[] list)
	{
		int root = begin;
		
		while (root*2+1 <= end)
		{
			int child = root * 2 + 1;
			
			if (child+1 <= end)
			{
				if (list[child] < list[child+1])
					child++;
			}
			
			if (list[root] < list[child])
			{
				int tmp = list[root];
				
				list[root] = list[child];
				list[child] = tmp;
				
				root = child;
				
			}
			else
				return;
		}
	}
}
